﻿using ShopLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShopApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DanePrzykladowe();
        }

        private Shop shop = new Shop();

    
        private void DanePrzykladowe()
        {
            shop.RegisterClient("Jan", "Kowalski",
                                "jk@example.com");
        }

        private void OdswiezKlientow()
        {
            listBoxKlienci.BeginInit();
            listBoxKlienci.Items.Clear();
            foreach (Client client in shop.Clients)
            {
                listBoxKlienci.Items.Add(client);
            }
            listBoxKlienci.EndInit();
        }

     
      
        private void buttonRejestrujKlienta_Click(object sender, RoutedEventArgs e)
        {
            shop.RegisterClient(textBoxImie.Text,
                               textBoxNazwisko.Text, textBoxAdres.Text);
            OdswiezKlientow();
        }

        private void buttonUsunKlienta_Click(object sender, RoutedEventArgs e)
        {  //object obj = listBoxKlienci.SelectedItem;
            //Client client = (Client)obj;

            //Client client = listBoxKlienci.SelectedItem as Client;
            //if (client != null)
            if (listBoxKlienci.SelectedItem is Client client)
            {
                shop.RemoveClient(client);
                OdswiezKlientow();
            }
            else
                MessageBox.Show("Nie można wykonać operacji");

        }

        private void buttonOdswiezKlienci_Click(object sender, RoutedEventArgs e)
        {
            OdswiezKlientow();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            OdswiezKlientow();
        }
    }
}
